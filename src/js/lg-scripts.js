(function($) {
  $(document).ready(function(){
    if($('.navbar-toggle').css('display') == 'none'){
    	$('.menu-item-has-children').on('mouseover', function(e){
    		console.log('haha');
            $(this).find('.dropdown-menu').fadeIn();
    	});

    	$('.menu-item-has-children').on('mouseleave', function(e){
    		e.preventDefault();
            $(this).find('.dropdown-menu').css('display','none');
    	});
    }

    //Testimonial Page
      $(window).load(function(){
        //Testimonial Grid
        if(typeof Masonry != "undefined") {
          new Masonry('.grid', {
            // options...
            itemSelector: '.grid-item',
          });
        }
      });

    //Single service Projects tab
    $('.current-projects .tab a').on('click', function(){
        var tab_content = $(this).attr('tab');

        $(this).addClass('active').closest('li').siblings().find('a').removeClass('active');
        $('.tab-content[tab="'+tab_content+'"]').addClass('active').siblings().removeClass('active');
    });

    if($('.single-service')[0] && !$('.menu-item-object-service').hasClass('active')){
        $('.menu-item-object-service').addClass('active');
    }
 });
}(jQuery));
