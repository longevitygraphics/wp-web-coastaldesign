<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */

?>

</div><!-- #content -->

	<!-- Hide on testimonials page -->
	<?php if($post->post_name != 'testimonials' && $post->post_name != 'careers'): ?>
	<div class="testimonial block center">
		<div class="container">
			<?php echo do_shortcode(get_field('testimonial', 'option')); ?>
			<a href="/testimonials" class="view-testimonial">VIEW TESTIMONIALS</a>
		</div>
	</div>
	<?php endif; ?>

	<footer id="colophon" class="site-footer">
		<div class="bg-brand-green clearfix pb-lg pt-lg">
			<div class="nav-footer">
				<?php // get_template_part("/inc/nav-footer"); ?>
			</div>
			<div class="footer-brand">
				<?php echo do_shortcode('[lg-site-logo]'); ?> 
				<?php echo get_field('footer_blurb', 'option'); ?>
			</div>

			<div class="blog-feed">
				<div class="east">
					<?php get_template_part("/inc/address-card-footer"); ?>
					<?php get_template_part("/inc/location-list"); ?>
				</div>
				<div class="west">
					<?php get_template_part("/inc/service-list"); ?>
				</div>
			</div>
			
			<div class="footer-utility">
				<div class="bbb">
					<a target="_blank" href="<?php echo get_field('footer_bbb', 'option')['url']; ?>"><img src="<?php echo get_field('footer_bbb', 'option')['image']['url']; ?>" alt="<?php echo get_field('footer_bbb', 'option')['image']['alt']; ?>"></a>
					<a href="<?php echo get_field('footer_bbb', 'option')['url']; ?>" class="review">Click here for review</a>
					<a class="cipi" href="https://www.icpi.org/resource-library/find-contractor/coastal-design-landscaping-inc" class="review"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/ICPI-CPI-Certified-Installer-logo.png" alt=""></a>
				</div>
				<div class="footer-social">
					<h2 class="h4">Social Media</h2>
					<?php echo do_shortcode('[lg-social-media]');?>
				</div>
			</div>
		</div>
		<div class="bg-brand-darkgreen clearfix bottom-bar">
			<div class="site-info"><?php get_template_part("/inc/site-info"); ?></div>
			<div class="site-longevity"> <?php get_template_part("/inc/site-footer-longevity"); ?> </div>
		</div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
