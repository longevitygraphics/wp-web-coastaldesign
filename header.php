<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */

?>  
<!doctype html>

<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', '_s' ); ?></a>

	<header id="masthead" class="site-header">
		<div class="bg-brand-green">
				<?php get_template_part( '/inc/options-locations' ); ?>
		</div>

		<div class="header-main">
			<section class="site-email"><a class="btn btn-link" href="mailto:<?php echo do_shortcode('[lg-email]'); ?>"><i class="fa fa-envelope fa-2x" aria-hidden="true"></i>&nbsp;<span class="header-email"><?php echo do_shortcode('[lg-email]'); ?></span></a></section>
			<div class="site-branding"> <?php echo do_shortcode('[lg-site-logo]'); ?> </div>
			<section class="site-phone">
				<a class="btn btn-link" href="tel:<?php echo do_shortcode('[lg-phone-main]'); ?>">
					<div class="site-phone-header"><i class="fa fa-phone-square" aria-hidden="true"></i></div>
					<div class="site-phone-body">
						<span class="site-phone-title">Free Consultation</span>
						<span class="site-phone-call">Call today:</span> <?php echo format_phone(do_shortcode('[lg-phone-main]')); ?>
					</div>
				</a>
			</section>
		</div>

		<?php  get_template_part("/inc/nav-main"); ?>
	</header>
