<?php
/**
 * About Page
 */

get_header(); ?>
<div id="content" class="site-content">
	<div id="primary" class="content-area single-service">
		<main id="main" class="site-main">

			<?php get_template_part('/inc/featured-slider');?>

			<!-- Intro Section -->
			<?php
				$intro_title = get_field('intro_title');
				$intro_description = get_field('intro_description');
			?>

				<div class="block container center">
					<?php if($intro_title): ?>
					<h1 class="h2 grass-icon">
						<?php echo $intro_title; ?>
					</h1>
					<?php endif; ?>
					<?php echo $intro_description; ?>
				</div>
			<!-- end Intro Section -->

			<?php get_template_part( 'template-parts/single-service-projects' ); ?>

			<!-- CTA -->
				<?php get_template_part('/template-parts/cta-area'); ?>
			<!-- end CTA -->

			<!-- Bottom Slider -->
			<?php
				$slider = get_field('slider');
			?>

				<div class="block container center">
					<?php echo do_shortcode($slider); ?>
				</div>
			<!-- end Bottom Slider -->

		</main>
	</div>
</div>
<?php get_footer();
