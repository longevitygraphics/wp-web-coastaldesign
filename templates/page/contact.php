<?php
/**
 * Contact Page
 */

get_header(); ?>
<div id="content" class="site-content">
	<div id="primary" class="content-area contact-page">
		<main id="main" class="site-main">

			<?php
				get_template_part('/inc/featured-slider');
			?>

			<div class="block container center">
				<h1 class="h2 grass-icon">Thank you for your interest in Coastal Design Landscaping.</h1>
				<div class="contact-info">
					<div class="contact">
						<div class="info">
							<div class="intro-blurb">
								<?php the_field('intro_blurb'); ?>
							</div>
							<h2><?php echo do_shortcode('[lg-contact-person]'); ?>, <?php echo do_shortcode('[lg-job-title]'); ?></h2>
							<section class="site-email"><a class="btn btn-link" href="mailto:<?php echo do_shortcode('[lg-email]'); ?>"><i class="fa fa-envelope" aria-hidden="true"></i><span class="header-email"><?php echo do_shortcode('[lg-email]'); ?></span></a></section>
							<section class="site-phone">
								<a class="btn btn-link" href="tel:<?php echo do_shortcode('[lg-phone-main]'); ?>">
									<div class="site-phone-header"><i class="fa fa-phone-square" aria-hidden="true"></i></div>
									<div class="site-phone-body">
										<span class="site-phone-title">Free Consultation</span>
										<span class="site-phone-call">Call today:</span> <?php echo format_phone(do_shortcode('[lg-phone-main]')); ?>
									</div>
								</a>
							</section>
							<!--<section class="site-fax"><a class="btn btn-link" href="tel:<?php echo do_shortcode('[lg-fax]'); ?>"><i class="fa fa-fax" aria-hidden="true"></i><span class="header-fax"><?php echo format_phone(do_shortcode('[lg-fax]')); ?></span></a></section>-->
						</div>
					</div>
					<div class="contact-form">
						<?php echo do_shortcode(get_field('contact_form')); ?>
					</div>
				</div>
			</div>

		</main>
	</div>
</div>
<?php get_footer();
