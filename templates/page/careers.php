<?php
/**
 * Careers Page
 */

get_header(); ?>
<div id="content" class="site-content">
	<div id="primary" class="content-area career-page">
		<main id="main" class="site-main">

			<?php
				$intro_title = get_field('intro_title');
				$content = get_field('content');
				$cta = get_field('cta');
				$background_image = get_field('background_image');
			?>

			<div class="block intro-block center" style="background-image: url(<?php echo $background_image; ?>);">
				<div class="container">
					<h1 class="h2">
						<?php echo $intro_title; ?>
					</h1>
					<div class="split-content">
						<div class="half-image">
							
						</div>
						<div class="half-copy">
							<?php echo $content; ?>
							<a class="cta" href="<?php echo $cta['url']; ?>"><?php echo $cta['text']; ?></a>
						</div>
					</div>
				</div>
			</div>

			<div class="block intro-block center">
				<div class="container">
					<h2 class="h2">Current Openings</h2>
					<?php
						$args = array(
				            'showposts'	=> -1,
				            'post_type'		=> 'career',
				        );
				        $result = new WP_Query( $args );

				        // Loop
				        if ( $result->have_posts() ) :
				        	?>
				        	<ol class="careers">
				        	<?php
				            while( $result->have_posts() ) : $result->the_post();
				            $pdf = get_field('job_pdf');
				        	?>
				        		<li>
				        			<?php echo get_the_title(); ?>  <a target="_blank" href="<?php echo $pdf; ?>"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a>
				        		</li>
							<?php
				            endwhile;
				            ?>
				            </ol>
				            <?php
				        endif; // End Loop

				        wp_reset_query();
					?>
				</div>
			</div>

		</main>
	</div>
</div>
<?php get_footer();
