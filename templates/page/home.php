<?php
/**
 * Default Page Template. Fixed width, no sidebar
 */

get_header(); ?>
<div id="content" class="page-home site-content">
	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<!-- Home Slider -->
			<?php
				$title_white = get_field('title_white');
				$cta_text = get_field('cta_text');
				$cta_link = get_field('cta_link');
			?>

			<div class="home-slider">
				<?php masterslider(1); ?>
				<div class="layer">
					<div class="text-layer">
						<div><?php echo $title_white; ?></div>
						<div class="headline-animation">
							<div>SPACE</div>
							<div>OUTSIDE</div>
						</div>
						<a class="cta" href="<?php echo $cta_link; ?>"><?php echo $cta_text; ?></a>
					</div>
					
				</div>
			</div>
			<!-- end Home Slider -->

			<!-- Intro Section -->
			<?php
				$intro_title = get_field('intro_title');
				$intro_description = get_field('intro_description');
			?>
			<div class="block container center">
				<?php if($intro_title): ?>
					<h1 class="h2 grass-icon"><?php echo $intro_title; ?></h1>
				<?php endif; ?>

				<?php if($intro_description): ?>
					<?php echo $intro_description; ?>
				<?php endif; ?>
			</div>
			<!-- end Intro Section -->

			<!-- Featured Services -->
			<?php
			$featured_services_background_image = get_field('featured_services_background_image');
			if( have_rows('featured_service') ):
				?>
				<div class="featured-services block center" style="background: linear-gradient( rgba(255, 255, 255, 0.5), rgba(255, 255, 255, 0.5) ),url(<?php echo $featured_services_background_image; ?>);">
					<div class="container">
				<?php
			    while ( have_rows('featured_service') ) : the_row();
			        $thumbnail = get_sub_field('thumbnail');
			        $title = get_sub_field('title');
			        $description = get_sub_field('description');
			        $link = get_sub_field('link');
			        ?>
						<div>
							<div class="featured-image">
								<img src="<?php echo $thumbnail['url']; ?>" alt="<?php echo $thumbnail['alt']; ?>">
							</div>
							<?php if($title): ?>
							<h3><?php echo $title; ?></h3>
							<?php endif; ?>
							<?php if($title): ?>
							<div class="featured-description"><?php echo $description; ?></div>
							<?php endif; ?>
							<a href="<?php echo $link; ?>" class="featured-link">READ MORE</a>
						</div>
			        <?php
			    endwhile;
			    ?>
			    	</div>
			    </div>
			    <?php
			else :
			    // no rows found
			endif;
			?>
			<!-- end Featured Services -->

			<!-- Our Services -->
			<?php
				$our_services_title = get_field('our_services_title');
			?>
				<div class="our-services block container center">
					<h2 class="grass-icon h2"><?php echo $our_services_title; ?></h2>
					<?php
						$args = array(
				            'showposts'	=> -1,
				            'post_type'		=> 'service',
				        );
				        $result = new WP_Query( $args );

				        // Loop
				        if ( $result->have_posts() ) :
				        	?>
				        	<ul class="services check-icon">
				        	<?php
				            while( $result->have_posts() ) : $result->the_post();
				            
				        	?>
				        		<li><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></li>
							<?php
				            endwhile;
				            ?>
				            </ul>
				            <?php
				        endif; // End Loop

				        wp_reset_query();
					?>
				</div>
			<!-- end Our Services -->

			<!-- Half Section 1 -->
			<?php
				$half_section_1_slider = get_field('half_section_1_slider');
				$half_section_1_content = get_field('half_section_1_content');
				$half_section_1_background_image = get_field('half_section_1_background_image');
				$half_section_1_cta_text = get_field('half_section_1_cta_text');
				$half_section_1_cta_link = get_field('half_section_1_cta_link');
			?>
			<div class="home-split-1 block center" style="background-image: url('<?php echo $half_section_1_background_image; ?>');">
				<div class="split-content container">
					<div class="half-image">
						<?php masterslider(2); ?>
					</div>
					<div class="half-copy">
						<?php echo $half_section_1_content; ?>
						<a href="<?php echo $half_section_1_cta_link; ?>" class="cta"><?php echo $half_section_1_cta_text; ?></a>
					</div>
				</div>
			</div>
			<!-- end Half Section 1 -->

			<!-- Half Section 2 -->
			<?php
				$half_section_2_image = get_field('half_section_2_image');
				$half_section_2_content = get_field('half_section_2_content');
			?>
			<div class="home-split-2 split-content reverse">
				<div class="half-image">
					<img src="<?php echo $half_section_2_image['url']; ?>" alt="<?php echo $half_section_2_image['alt']; ?>">
				</div>
				<div class="half-copy">
					<?php echo $half_section_2_content; ?>
					<?php
						$args = array(
				            'showposts'	=> -1,
				            'post_type'		=> 'location',
				        );
				        $result = new WP_Query( $args );

				        // Loop
				        if ( $result->have_posts() ) :
				        	?>
				        	<ul>
				        	<?php
				            while( $result->have_posts() ) : $result->the_post();
				        	?>
				        		<li><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?> Landscaping Design</a></li>
							<?php
				            endwhile;
				            ?>
				            </ul>
				            <?php
				        endif; // End Loop

				        wp_reset_query();
					?>
				</div>
			</div>
			<!-- end Half Section 2 -->

			<!-- Our Projects -->
			<?php 
				$our_projects_title = get_field('our_projects_title');
			?>
			<div class="our-projects block container center">
				<?php if($our_projects_title): ?>
				<h2 class="grass-icon h2"><?php echo $our_projects_title; ?></h2>
				<?php endif; ?>

				<?php
				if( have_rows('projects') ):
					?>
					<ul class="our-projects">
					<?php
				    while ( have_rows('projects') ) : the_row();
				        $image = get_sub_field('image');
				        $title = get_sub_field('title');
				        $description = get_sub_field('description');
				        $cta = get_sub_field('cta');
				        ?>
							<li>
			        			<a href="<?php echo $cta->guid; ?>"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>"></a>
			        			<div class="project-description">
			        				<?php if($title): ?>
									<h3><?php echo $title; ?></h3>
			        				<?php endif; ?>
			        				
			        				<?php echo $description; ?>
			        				<a href="<?php echo $cta->guid; ?>" class="read-more">READ MORE</a>
			        			</div>
			        		</li>
				        <?php
				    endwhile;
				    ?>
				    </ul>
				    <?php
				else :
				    // no rows found
				endif;
				?>
			</div>
			<!-- start Our Projects -->

		</main>
	</div>
</div>
<?php get_footer();
