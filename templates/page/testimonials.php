<?php
/**
 * About Page
 */

get_header(); ?>
<div id="content" class="site-content">
	<div id="primary" class="content-area testimonial-page">
		<main id="main" class="site-main">

			<div class="block container center">
				<h1 class="h2">What our clients say</h1>
				<?php
					$args = array(
			            'showposts'	=> -1,
			            'post_type'		=> 'lg_testimonial',
			        );
			        $result = new WP_Query( $args );

			        // Loop
			        if ( $result->have_posts() ) :
			        	?>
			        	<div class="grid">
			        	<?php
			            while( $result->have_posts() ) : $result->the_post();
			            $author = get_field('author');
			            $content = get_field('content');
			            $job_title = get_field('job_title');
			        	?>
			        		<div class="grid-item">
			        			<div>
			        				<?php if($author): ?>
										<h2 class="author"><?php echo $author; ?></h2>
			        				<?php endif; ?>
			        				<?php if($job_title): ?>
										<div class="job-title"><?php echo $job_title; ?></div>
			        				<?php endif; ?>

									<?php if($content): ?>
										<div class="content">
											<?php echo $content; ?>
										</div>
									<?php endif; ?>
			        			</div>
			        		</div>
						<?php
			            endwhile;
			            ?>
			            </div>
			            <?php
			        endif; // End Loop

			        wp_reset_query();
				?>
			</div>
			
		</main>
	</div>
</div>
<?php get_footer();