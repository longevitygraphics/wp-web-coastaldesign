<?php


add_action('after_setup_theme','theme_start', 16);

function theme_start(){
	add_template_support();
}

/*********************
	THEME SUPPORT
*********************/

// Adding WP 3+ Functions & Theme Support
function add_template_support() {

	// wp thumbnails (sizes handled in functions.php)
	add_theme_support('post-thumbnails');

	// rss
	add_theme_support('automatic-feed-links');

	//custom logo

	add_theme_support( 'custom-logo', array(
		'flex-height' => true,
		'flex-width'  => true,
		'header-text' => array( 'site-title', 'site-description' ),
	) );

	// wp menus
	add_theme_support( 'menus' );
	register_nav_menu( "top-nav", "Top Nav Menu(top-nav)" );
	register_nav_menu( "bottom-nav", "Bottom Nav Menu(bottom-nav)" );

	//html5 support (http://themeshaper.com/2013/08/01/html5-support-in-wordpress-core/)
	add_theme_support( 'html5',
	         array(
	         	'comment-list',
	         	'comment-form',
	         	'search-form',
	         )
	);

	function add_svg_to_upload_mimes( $upload_mimes ) {
		$upload_mimes['svg'] = 'image/svg+xml';
		$upload_mimes['svgz'] = 'image/svg+xml';
		return $upload_mimes;
	}
	add_filter( 'upload_mimes', 'add_svg_to_upload_mimes', 10, 1 );


} /* end bridgemachine theme support */

function lg_enqueue_styles_scripts() {

    // Child theme style sheet setup.
    $parent_style = '_s-style';
    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/style.css', array( $parent_style ), 1.0 );
    wp_enqueue_style( 'normalize', 'https://cdnjs.cloudflare.com/ajax/libs/normalize/7.0.0/normalize.min.css', false );
	// Fonts
	wp_enqueue_style( 'wpb-google-fonts', 'https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i|Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i', false );

	// Register Scripts
	wp_register_script( 'bootstrapjs', get_stylesheet_directory_uri() . "/js/bootstrap.min.js", array('jquery'), '3.3.7' );

	wp_register_script( 'lg-scripts', get_stylesheet_directory_uri() . "/js/lg-scripts-min.js", array('jquery', 'masonryjs'), false );
	wp_register_script( 'vendor', get_stylesheet_directory_uri() . "/js/vendor.min.js", array('jquery'), false );
	if ( is_page( 'testimonials' ) ) {
		wp_register_script( 'masonryjs', "https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js", array('jquery'), false );
		wp_enqueue_script( 'masonryjs' );
	}
	// Enqueue Scripts.
	wp_enqueue_script( 'bootstrapjs' );

	wp_enqueue_script( 'lg-scripts' );
	wp_enqueue_script( 'vendor' );
}
add_action( 'wp_enqueue_scripts', 'lg_enqueue_styles_scripts' );


//Dequeue JavaScripts
function project_dequeue_unnecessary_scripts() {
	wp_dequeue_script( '_s-navigation' );
	wp_deregister_script( '_s-navigation' );
}
add_action( 'wp_print_scripts', 'project_dequeue_unnecessary_scripts' );

// Format Phone Numbers Function
function format_phone($phone)
{
	$phone = preg_replace("/[^0-9]/", "", $phone);

	if(strlen($phone) == 7)
		return preg_replace("/([0-9]{3})([0-9]{4})/", "$1-$2", $phone);
	elseif(strlen($phone) == 10)
		return preg_replace("/([0-9]{3})([0-9]{3})([0-9]{4})/", "$1-$2-$3", $phone);
	else
		return $phone;
}

// Format Phone Numbers Function
function format_phone_dot($phone)
{
	$phone = preg_replace("/[^0-9]/", "", $phone);

	if(strlen($phone) == 7)
		return preg_replace("/([0-9]{3})([0-9]{4})/", "$1-$2", $phone);
	elseif(strlen($phone) == 10)
		return preg_replace("/([0-9]{3})([0-9]{3})([0-9]{4})/", "$1.$2.$3", $phone);
	else
		return $phone;
}

// Register Custom Navigation Walker
require_once(get_stylesheet_directory() . '/inc/wp-bootstrap-navwalker.php');

// This theme uses wp_nav_menu() in one location.
register_nav_menus( array(
	'footer-1' => esc_html__( 'Footer', '_s' ),
) );

function costaldesign_cms(){
	require_once(get_stylesheet_directory() . '/template-parts/client-instruction.php');
}

function coastaldesign_menu() {
    add_menu_page(
        __( 'Coastal Design', 'coastal-design' ),
        'Coastal Design',
        'manage_options',
        'coastal-design',
        '',
        'dashicons-admin-site',
        2
    );

	add_submenu_page(
		'coastal-design',
		__('Client Instruction'),
		__('Client Instruction'),
		'edit_themes',
		'theme_cms',
		'costaldesign_cms'
	);

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Site Share Content',
		'menu_title'	=> 'Site Share Content',
		'parent_slug'	=> 'coastal-design',
	));
}

add_action( 'admin_menu', 'coastaldesign_menu' );

//Taxonomy
function create_taxonomy(){
	//Blog Category
	/*register_taxonomy(
		'blog-category',
		'blog',
		array(
			'label' => __( 'Category' ),
			'rewrite' => array( 'slug' => 'blog-category' ),
			'hierarchical' => true,
		)
	);*/
}

add_action( 'init', 'create_taxonomy' );


//Custom Post Types
function create_post_type() {

	// LOCATIONS
	register_post_type( 'location',
		array(
		  'labels' => array(
		    'name' => __( 'Locations' ),
		    'singular_name' => __( 'Location' )
		  ),
		  'public' => true,
		  'has_archive' => false,
		  'menu_icon'   => 'dashicons-location',
		  'show_in_menu'	=> 'coastal-design',
		  'supports' => array( 'thumbnail','title', 'editor', 'excerpt' )
		)
	);

    // SERVICES
  	register_post_type( 'service',
        array(
            'labels' => array(
                'name'          => __( 'Services' ),
                'singular_name' => __( 'Service' )
            ),
            'public'      => true,
            'has_archive' => false,
            'menu_icon'   => 'dashicons-admin-tools',
            'show_in_menu'	=> 'coastal-design',
            'supports' => array( 'thumbnail','title', 'editor', 'excerpt' ),
        )
    );

    // PROJECTS
  	register_post_type( 'project',
        array(
            'labels' => array(
                'name'          => __( 'Projects' ),
                'singular_name' => __( 'Project' )
            ),
            'public'      => true,
            'has_archive' => false,
            'menu_icon'   => 'dashicons-list-view',
            'show_in_menu'	=> 'coastal-design',
            'supports' => array( 'title', 'editor', 'thumbnail', 'revisions'),
        )
    );
    // Careers
  	register_post_type( 'career',
        array(
            'labels' => array(
                'name'          => __( 'Careers' ),
                'singular_name' => __( 'Career' )
            ),
            'public'      => true,
            'has_archive' => false,
            'menu_icon'   => 'dashicons-list-view',
            'show_in_menu'	=> 'coastal-design',
            'supports' => array( 'title', 'editor', 'thumbnail', 'revisions'),
        )
    );
}
add_action( 'init', 'create_post_type' );

require_once(get_stylesheet_directory() . '/functions/template-override.php');

?>
