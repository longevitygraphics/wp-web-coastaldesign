
<!-- Default Address Stuff -->
<div class="address-card">
	<?php the_custom_logo(); ?>

	<address>

		<span class="card-map-marker">
			<span><?php echo do_shortcode('[lg-address1]'); ?></span><br>
			<span><?php echo do_shortcode('[lg-city]'); ?></span>, <span><?php echo do_shortcode('[lg-province]'); ?></span>&nbsp;<span><?php echo do_shortcode('[lg-postcode]'); ?></span><br>
		</span>

		<br>

		<?php $phone = get_field('company_phone', 'option'); ?>
		<span class="card-map-phone">Phone: <a href="tel:+1<?php echo do_shortcode('[lg-phone-main]'); ?>"><?php echo format_phone(do_shortcode('[lg-phone-main]')); ?></a></span><br>
		<span class="card-map-phone">Sales: <a href="tel:+1<?php echo do_shortcode('[lg-phone-alt]'); ?>"><?php echo format_phone(do_shortcode('[lg-phone-alt]')); ?></a></span><br>

	</address>

</div>

