<section class="locations-utility hidden-xs">
		<?php
			$args = array(
	            'showposts'	=> -1,
	            'post_type'		=> 'location',
	        );
	        $result = new WP_Query( $args );

	        // Loop
	        if ( $result->have_posts() ) :
	        	?>
	        	<ul>
	        	<?php
	            while( $result->have_posts() ) : $result->the_post();
	        	?>
						<li><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></li>
				<?php
	            endwhile;
	            ?>
	            </ul>
	            <?php
	        endif; // End Loop

	        wp_reset_query();
		?>
</section>









