<div class="address-card">
	<h2 class="h4">Contact us</h2>
	<address>
		Contact: <?php echo do_shortcode('[lg-contact-person]'); ?>,&nbsp;<?php echo do_shortcode('[lg-job-title]'); ?><br>
		<span class="card-map-phone" >Tel: <a href="tel:+1<?php echo do_shortcode('[lg-phone-main]'); ?>"><?php echo format_phone_dot(do_shortcode('[lg-phone-main]')); ?></a></span><br>
		<span class="card-map-phone">Email: <a href="mailto:<?php echo do_shortcode('[lg-email]'); ?>"><?php echo do_shortcode('[lg-email]'); ?></a></span><br>
	</address>
</div>

