<div class="location-footer">
	<h2 class="h4">Locations we serve</h2>

	<?php $wpb_all_query = new WP_Query(array('post_type'=>'location', 'post_status'=>'publish', 'posts_per_page'=>-1)); ?>
	 
	<?php if ( $wpb_all_query->have_posts() ) : ?>
		<ul class="list-unstyled">
		    <?php while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post(); ?>
		        <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
		    <?php endwhile; ?>
		</ul>
	<?php endif; ?>
</div>