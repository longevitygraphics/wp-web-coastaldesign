<?php 
	$args = array( 
		'post_type' => 'post', 
		'posts_per_page' => 2 
	);

	$loop = new WP_Query( $args );
?>
<div class="feed">
	<h2>Blog Feed</h2>
	<?php while ( $loop->have_posts() ) : $loop->the_post()  ?>

		<div class="panel panel-default">
			<div class="panel-heading"><span class="h4"><?php the_title(); ?></span></div>
			<div class="panel-body entry-content"> <?php the_excerpt(); ?> </div>
			<div class="panel-footer text-right"><a class="btn btn-default" href="<?php echo get_permalink(); ?>">Learn More</a></div>
		</div>

	<?php endwhile; ?>
</div>