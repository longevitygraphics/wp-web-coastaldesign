<?php
	$page_headline = get_field('feature_slider_headline');
	$featured_slider = get_field('feature_slider');
?>

<div class="feature-banner">
	<?php if($featured_slider): ?>
		<?php echo do_shortcode($featured_slider); ?>
	<?php endif; ?>

	<?php if($page_headline): ?>
	<h2 class="headline">
		<?php echo $page_headline; ?>
	</h2>
	<?php endif; ?>
</div>