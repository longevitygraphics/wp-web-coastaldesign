<?php
	$cta_copy = get_field('cta_copy', 'option');
	$cta_background_image = get_field('cta_background_image', 'option');
	$cta_button = get_field('cta_button', 'option');
	//Group - button_text, button_link
?>

<div class="site-cta block" style="background-image: url(<?php echo $cta_background_image; ?>);">
	<div class="container">
		<div class="copy">
			<?php if($cta_copy): ?>
				<h2><?php echo $cta_copy; ?></h2>
			<?php endif; ?>
		</div>
		<div class="cta-button">
			<a href="<?php echo $cta_button['button_link']; ?>"><?php echo $cta_button['button_text']; ?></a>
		</div>
	</div>
</div>